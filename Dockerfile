# syntax=docker/dockerfile:1
FROM ubuntu:20.04
ADD setup.sh /setup.sh
RUN /setup.sh
ENV PATH="/root/.cargo/bin:${PATH}"
